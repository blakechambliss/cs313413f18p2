From TestIterator.java:

        // TODO also try with a LinkedList - does it make any difference?
        //BC: No. Same amount of tests fail.

        // TODO what happens if you use list.remove(Integer.valueOf(77))?
        //BC: No change in failed test status

From TestPerformance.java:

        //Which of the two lists performs better as the size increases?
        //BC: Array List

        //RESULTS

        //Size =10, REPS = 1Mil
        //    <testcase name="testArrayListAccess" classname="cs271.lab.list.TestPerformance" time="0.01"/>
        //  <testcase name="testLinkedListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.049"/>
        //  <testcase name="testLinkedListAccess" classname="cs271.lab.list.TestPerformance" time="0.019"/>
        //  <testcase name="testArrayListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.027"/>

        //Size =100, REPS = 1Mil
        //  <testcase name="testArrayListAccess" classname="cs271.lab.list.TestPerformance" time="0.01"/>
        //  <testcase name="testLinkedListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.041"/>
        //  <testcase name="testLinkedListAccess" classname="cs271.lab.list.TestPerformance" time="0.033"/>
        //  <testcase name="testArrayListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.049"/>

        //Size =1000, REPS = 1Mil
        //  <testcase name="testArrayListAccess" classname="cs271.lab.list.TestPerformance" time="0.011"/>
        //  <testcase name="testLinkedListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.041"/>
        //  <testcase name="testLinkedListAccess" classname="cs271.lab.list.TestPerformance" time="0.421"/>
        //  <testcase name="testArrayListAddRemove" classname="cs271.lab.list.TestPerformance" time="0.194"/>

From TestList.java:

        // TODO also try with a LinkedList - does it make any difference?
        //BC: no it doesn't change the tests failed.